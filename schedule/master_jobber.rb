#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'
require 'logger'

#logger = Logger.new(STDERR)
#logger = Logger.new(STDOUT)
#file = File.open('/echoware/master_jobber.log', File::WRONLY | File::APPEND)
logger = Logger.new('/echoware/master_jobber.log', 'weekly')

logger.debug {"------------->master_jobber.rb start"}

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	#If I am already running' don't run.

	switch = Switch[1] 	#Master_Jobber
	if switch.swi_running == 1 # 1 = true
		Log.create.logme_error(1,4,16,nil,nil,nil,'Skipping this master_jobber' ) #Level, Type, Message, Job, LinkType,Customer,More)
		exit
	else
		switch.start()	
	end

	jobs = master[:job].filter(:job_jos_id => 1)	#Retrieve those with a status of queued
	begin
logger.debug {"Number of Jobs: #{jobs.count}"}

	jobs=jobs.all	#Get them into Jobs

	jobs.each do |tjob|	#For Each Job Run the scripts

		job = Job[tjob[:job_id]]
logger.debug {"Job to Class: #{job.job_id}"}
		format = Format[job.job_for_id]	#Get the format in order to get the format_version script
logger.debug {"format to class: #{format.for_id}"}
		format_version = Format_version[format.get_max_fvs_id(master)]		# Could be an issues!!!! If we want to allow for old versions to be used at any time

		begin
		job.running()
			if format_version.fvs_fvi_id == 1 #If it is a script
				logger.debug {"ruby " +$path_format + format_version[:fvs_command] + ' ' + job[job.job_id].to_s()}
				system "ruby " +$path_format + format_version[:fvs_command] + ' ' + job.job_id.to_s()
				#if format.for_fot_id == 1  				#If an import
				#	job.move_file_to_storage(master)		
				#end

			elsif format_version.fvs_fvi_id == 2#If it is a stored procedure
			else
				puts "Log me; failed"
			end	
					
		rescue Exception => e
			Log.create.logme_error(3,4,6,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
			job.error()			
		end
	end
	rescue Exception => e
		Log.create.logme_error(3,4,6,nil,nil,nil,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
	end

result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
file = File.open("/echoware/master_jobber.html", "w+")
printer.print(file)
file.close

switch.end()	

logger.debug {"----------> Closed all files and END END"}


