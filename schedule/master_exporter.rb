#!/usr/bin/env ruby
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	begin

		export_id= ARGV[0] #Get the id of the export to be queued
		export=Export[export_id]#Export record stored

		Job.create().newjob(2,export.exp_for_id,export.exp_cus_id,export.exp_id,nil) #Type,Format,Customer,LinkID,Raw_File_Name

	rescue Exception => e
		Log.create.logme_error(2,7,11,nil,export.exp_id,export.exp_cus_id, "Error in master_exporter.rb | " + e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
	end



