#-----------------------------------------------------------------------------------------------------------------------
# 3/22/2012 -- Adam Perkins
# Purpose: This file is set on a cron job.  Each time it is run it manages the batching process.  
# A batch is a group of imports that must be completed in order for the translation process to run
# This could be a one to one relationship or many to one.
# Example:
#   1. Receive a single import from paychex with benefit information.  One import.  One translation function.  One batch. One Job.  One import.
#   2. Receive a dozen imports from payroll systems around the world.  Cannot run aggregate reporting without ALL imports being completed or bypassed.  One batch, 12 jobs.
#
#-----------------------------------------------------------------------------------------------------------------------
#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'


RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	batches = master[:batch].filter(:bat_date_end => nil).or(:bat_date_end.identifier > Time.now) #Retrieve active batches

	#Step 1 = From all active batches create new batch_instances based on parameters and date from previous batch_instance.  IF no previous batch_instance use BATCH to create the first batch_instance
	batches.each do |tbatch|
		batch = Batch[tbatch[:bat_id]]

		batch_instances = master[:batch_instance].where(:bai_bat_id => batch.bat_id).and(:bai_date_end.identifier > Time.now()) #Get batch instances for current time period

		batch_instance = ""
	
		if batch_instances.count == 0	#No current batch_instances are available.  Create a new batch instance
			period = 60*60*batch[:bat_window_hours]
			times = ((Time.now - batch[:bat_date_start]) / (period)).floor	# Modulo equivalent
	
			#If the end date of the batch is less than the next end date of the instance.. update the instance to be to the end of the batch
			if (batch.bat_date_end == nil) or batch.bat_date_end > batch.bat_date_start+period*(times+1)
				bai_date_end = batch.bat_date_start+period*(times+1)
			else
				bai_date_end = batch.bat_date_end
			end

			batch_instance = Batch_instance.create(:bai_date_start=>batch[:bat_date_start]+period*times, :bai_bat_id => batch[:bat_id],:bai_bas_id=>1,:bai_date_end => bai_date_end)	
		else
			batch_instance = Batch_instance[batch_instances.first[:bai_id]]
		end
		
			if Time.now > batch_instance.bai_date_start && Time.now <= batch_instance.bai_date_end && (batch_instance.bai_bas_id == 1) #(Ready for Translation Batch)

				#Step 2 = If a batch_instance is successful (Each job inside of it is A-OK) Mark as A-ok.

#				puts "batId:" + batch.bat_id.to_s()
				imports = master[:import].filter(:imp_bat_id => batch.bat_id).filter(ok_date(:imp_date_end.identifier))
			
				total_jobs_in_batch = imports.count
#				puts total_jobs_in_batch

				imports = imports.join(:job, :job_link_id => :imp_id).filter(:job_jos_id=>3)
				imports = imports.filter(:job_date_completed.identifier > batch_instance.bai_date_start).and(:job_date_completed.identifier < batch_instance.bai_date_end) 				
				successful_jobs_this_batch_cycle = imports.count

				if (successful_jobs_this_batch_cycle == total_jobs_in_batch) && (total_jobs_in_batch != 0) #Batch is Gooooooood and complete.  Run the final translation format				
					batch_instance.complete
					#Step 5 = SChedule Job 
					Job.create().newjob(3,batch.bat_for_id,nil,batch_instance.bai_id,nil) #Type,Format,Customer,LinkID,Raw_File_Name

				end				
			end #End Check within Period and not marked as complete
	end

#Mark old batchs as error if they do not 'complete'
	batch_instances = master[:batch_instance].filter(:bai_bas_id => 1).and(:bai_date_end.identifier < Time.now)
	batch_instances.each do |tbatch_instance|
		batch_instance = Batch_instance[tbatch_instance[:bai_id]]
		batch_instance.error()
		
		Log.create.logme_error(2,6,9,nil,batch_instance.bai_bat_id,nil,'' ) #Level, Type, Message, Job, LinkType,Customer,More)

	end
