#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	jobs = master[:job].filter(:job_jos_id => 1)	#Retrieve those with a status of queued

	jobs=jobs.all	#Get them into Jobs

	jobs.each do |job|	#For Each Job Run the scripts
		j = Job[job[:job_id]]
	Log.create(:log_more => "Start", :log_date_created => Time.now.to_s())
		format = Format[job[:job_for_id]]	#Get the format in order to get the format_version script
		format_version = Format_version[format.get_max_fvs_id(master)]		# Could be an issues!!!! If we want to allow for old versions to be used at any time
		begin
	Log.create(:log_more => "Begin")
		j.running()
	Log.create(:log_more => j.job_id)
	Log.create(:log_more => "Format Version:" + format_version.fvs_fvi_id.to_s())
			if format_version.fvs_fvi_id == 1 #If it is a script

	Log.create(:log_more => "ruby " +$path_format + format_version[:fvs_command] + ' ' + job[:job_id].to_s())
				system "ruby " +$path_format + format_version[:fvs_command] + ' ' + job[:job_id].to_s()
				if format.for_fot_id == 1  				#If an import
					j.move_file_to_storage(master)
				end

			elsif format_version.fvs_fvi_id == 2#If it is a stored procedure
	Log.create(:log_more => "Uh u")
			else
				puts "Log me; failed"
			end	
		rescue Exception => e
#			puts e.message
			Log.create.logme_error(3,4,6,j.job_id,nil,j.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
			j.error()			
		end
	end
	Log.create(:log_more => "finish")

#	printer = RubyProf::FlatPrinter.new(result)
#	printer.print(STDOUT,0)


result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
file = File.open("/echoware/master_jobber.html", "w+")
printer.print(file)
file.close
