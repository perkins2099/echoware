#-----------------------------------------------------------------------------------------------------------------------
# 3/22/2012 -- Adam Perkins
# Purpose: This file is set on a cron job.  Each time it is run it manages the batching process.  
# A batch is a group of imports that must be completed in order for the translation process to run
# This could be a one to one relationship or many to one.
# Example:
#   1. Receive a single import from paychex with benefit information.  One import.  One translation function.  One batch. One Job.  One import.
#   2. Receive a dozen imports from payroll systems around the world.  Cannot run aggregate reporting without ALL imports being completed or bypassed.  One batch, 12 jobs.
#
#-----------------------------------------------------------------------------------------------------------------------
#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'


RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	#Steps to run on reflexive batches
	batches = master[:batch].filter(ok_date(:bat_date_end.identifier)).and(:bat_bay_id => 2) #Reflexive BAtches
	batches.each do |tbatch|
		batch = Batch[tbatch[:bat_id]]
		cus_id = master[:import].where(:imp_bat_id => batch.bat_id).first[:imp_cus_id]
		#Get batch instances associated with each batch (This means it could run 2 or 3 times on a single import process if it was picked up that man times
		batch_instances = master[:batch_instance].where(:bai_bat_id => batch.bat_id).and(:bai_bas_id =>4) 

		batch_instances.each do |t_bi|
			batch_instance = Batch_instance[t_bi[:bai_id]]
			export_schedules = master[:export_schedule].where(:esc_sct_id => 2).and(:esc_bat_id => batch.bat_id)
			export_schedules.each do |t_es|
				es = Export_schedule[t_es[:esc_id]]
				for_id = master[:export].where(:exp_id => es.esc_exp_id)[:exp_for_id]
				Job.create().newjob(2,for_id,cus_id,es.esc_exp_id,nil,batch_instance.bai_id) #Type,Format,Customer,LinkID,Raw_File_Name, bai_id
				batch_instance.export_queued
			end
		end		
		
	end

	#Steps to run on all other batches.  (Rewrite to run certain steps on certain status's of batch_instances)
	batches = master[:batch].filter(ok_date(:bat_date_end.identifier)).and(:bat_bay_id => 1) #Retrieve active batches which are scheduled..not reflexive

	#Step 1 = From all active batches create new batch_instances based on parameters and date from previous batch_instance.  IF no previous batch_instance use BATCH to create the first batch_instance
#	puts batches.count
	batches.each do |tbatch|
		begin
			batch = Batch[tbatch[:bat_id]]
			cus_id = master[:import].where(:imp_bat_id => batch.bat_id).first[:imp_cus_id]
			rescue Exception => e #No imports for this batch available.
				Log.create.logme_error(3,6,12,nil,batch.bat_id,nil,e.message + ' | ' + e.backtrace.inspect ) 

			batch_instances = master[:batch_instance].where(:bai_bat_id => batch.bat_id).and(:bai_date_end.identifier > Time.now()) #Get batch instances for current time period

			batch_instance = ""	#Declare this here for the next assignment
	
			if batch_instances.count == 0	#No current batch_instances are available.  Create a new batch instance
				period = 60*60*batch[:bat_window_hours]
				times = ((Time.now - batch[:bat_date_start]) / (period)).floor	# Modulo equivalent
	
				#If the end date of the batch is less than the next end date of the instance.. update the instance to be to the end of the batch
				if (batch.bat_date_end == nil) or batch.bat_date_end > batch.bat_date_start+period*(times+1)
					bai_date_end = batch.bat_date_start+period*(times+1)
				else
					bai_date_end = batch.bat_date_end
				end

				batch_instance = Batch_instance.create(:bai_date_start=>batch[:bat_date_start]+period*times, :bai_bat_id => batch[:bat_id],:bai_bas_id=>1,:bai_date_end => bai_date_end)	
			else
				batch_instance = Batch_instance[batch_instances.first[:bai_id]]
			end
		
			if Time.now > batch_instance.bai_date_start && Time.now <= batch_instance.bai_date_end && (batch_instance.bai_bas_id == 1) #(Ready for Translation Batch)
				imports = master[:import].filter(:imp_bat_id => batch.bat_id).filter(ok_date(:imp_date_end.identifier))
				total_jobs_in_batch = imports.count

				imports = imports.join(:job, :job_link_id => :imp_id).filter(:job_jos_id=>3)
				imports = imports.filter(:job_date_completed.identifier > batch_instance.bai_date_start).and(:job_date_completed.identifier < batch_instance.bai_date_end) 				
				successful_jobs_this_batch_cycle = imports.count
				if (successful_jobs_this_batch_cycle == total_jobs_in_batch) && (total_jobs_in_batch != 0) #Batch is Gooooooood and complete.  Run the final translation 
#					batch_instance.import_jobs_complete				
					#Step 5 = SChedule Job if it is needed (two level import)
					if bat_for_id != nil
						Job.create().newjob(3,batch.bat_for_id,cus_id,batch_instance.bai_id,nil,batch_instance_bai_id) #Type,Format,Customer,LinkID,Raw_File_Name, bai_id
					else
						batch_instance.all_jobs_complete
					end
				end				

			end #End Check within Period and not marked as complete
		rescue Exception => e #Generic Scheduling error
			Log.create.logme_error(3,6,8,nil,batch.bat_id,nil,e.message + ' | ' + e.backtrace.inspect ) 		

		end
	end

#Mark old batchs as error if they do not 'complete'
	batch_instances = master[:batch_instance].join(:batch, :bat_id => :bai_bat_id).filter(:bai_bas_id => 1).and(:bai_date_end.identifier < Time.now).and(:bat_bay_id => 1)
	batch_instances.each do |tbatch_instance|
		batch_instance = Batch_instance[tbatch_instance[:bai_id]]
		batch_instance.error()
		
		Log.create.logme_error(2,6,9,nil,batch_instance.bai_bat_id,nil,'' ) #Level, Type, Message, Job, LinkType,Customer,More)

	end
