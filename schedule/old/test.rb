#-----------------------------------------------------------------------------------------------------------------------
# 3/22/2012 -- Adam Perkins
# Purpose: This file is set on a cron job.  Each time it is run it manages the batching process.  
# A batch is a group of imports that must be completed in order for the translation process to run
# This could be a one to one relationship or many to one.
# Example:
#   1. Receive a single import from paychex with benefit information.  One import.  One translation function.  One batch. One Job.  One import.
#   2. Receive a dozen imports from payroll systems around the world.  Cannot run aggregate reporting without ALL imports being completed or bypassed.  One batch, 12 jobs.
#
#-----------------------------------------------------------------------------------------------------------------------
#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'


RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	batches = master[:batch].filter(:bat_date_end => nil).or(:bat_date_end.identifier > Time.now) 

batches.each do |tbatch|

puts tbatch[:bat_id]
end
