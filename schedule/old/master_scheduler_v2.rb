#!/usr/bin/env ruby
require 'cronedit'
include CronEdit
require "/echoware/code/db.rb"
require "mysql2"
require "rubygems"
require "sequel"
require 'ruby-prof'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	begin
		master = Mysql2::Client.new(:host => $host, :username => $username, :password => $password, :database =>$database ) 
		results = master.query("select * from v_get_changes_to_scheduled_exports")

	rescue Mysql2::Error => e
     			$LOG.error "Error code: #{e.errno}  Error message: #{e.error}"
			$LOG.error "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
	rescue 
			$LOG.error("ERROR Connecting to DB: : #{$database}")  
	end
	cur_time = Time.new

	#puts cur_time
	results.each do |result|
		#puts result
		esc_cron_pattern = result["esc_cron_pattern"]
		esc_action = result["esc_action"]
		esc_table_name = result["exp_table_name"]
		esc_date_start = result["esc_date_start"]
		esc_date_end =  result["esc_date_end"]
		esc_scs_id = result["esc_scs_id"]
		esc_name = result["esc_name"]
		esc_id =result["esc_id"]

		cron_paths = "/usr/bin/env"

		if esc_date_start <= cur_time
			if esc_date_end.nil? or esc_date_end > cur_time #Start the scheduling process for this scheduler 
				Crontab.Add  "#{esc_name}", "#{esc_cron_pattern} #{cron_paths} #{esc_action}"
				master.query("call update_export_schedule_status(#{esc_id}, 1)")
			elsif esc_date_end <= cur_time #Terminate the scheduling process for this scheduler
				Crontab.Remove  "#{esc_name}"
				master.query("call update_export_schedule_status(#{esc_id}, 2)") # Set the status to 2 = terminated
			else "error"
			end
		end



	end



result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
file = File.open("/echoware/master_scheduler.html", "w+")
printer.print(file)
file.close
