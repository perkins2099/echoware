#!/usr/bin/ruby 
#!/usr/bin/env ruby

require "rubygems"
require "find"
require "sequel"
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require 'ruby-prof'
require "time"

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	begin
		imports = master[:import].join(:format, :for_id => :imp_for_id).join(:customer, :cus_id => :import__imp_cus_id).filter(:imp_date_end => nil).or(:imp_date_end.identifier > Time.now)

		imports.each do |import|
	#		puts import[:cus_db]
			path =$path_ftp + "#{import[:cus_db]}/#{import[:imp_value]}"

			#If this directory exists
			if File::directory?( path )
				Find.find(path) do |raw_file_name|
					#If there is a file inside here
					if File::file?( raw_file_name )
						if ((Time.now - File::mtime(raw_file_name)) / 60 ) > (1)	#In Minutes
							new_file_name = get_import_file_name(raw_file_name,import)

							job = Job.create().newjob(1,import[:for_id],import[:imp_cus_id],import[:imp_id],new_file_name)#Type,Format,Customer,LinkID,file
							move_file_to_storage(raw_file_name,new_file_name,import[:imp_cus_id],master)		
						end
					end
				end
			end
		end
		rescue Exception => e
			Log.create.logme_error(3,8,14,nil,nil,nil,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
	end

result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
file = File.open("/echoware/master_ftp_scan.html", "w+")
printer.print(file)
file.close
