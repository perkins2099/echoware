#!/usr/bin/env ruby

require 'cronedit'
include CronEdit

#crontab -e (This is what should be on top of the assignment of the cron jobs
#SHELL=/bin/bash
#GEM_HOME=/usr/local/lib/ruby/gems/1.9.1
#PATH=/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
#HOME=/home/adam
#RUBYOPT=rubygems
#GEM_PATH=/usr/local/lib/ruby/gems/1.9.1


#These should run on opposing schedules to avoid confusion and partial job overlapping
Crontab.Add  "master_scheduler", "* * * * * /usr/bin/env ruby /echoware/code/schedule/master_scheduler.rb"	#Updates, halts, etc all scheduled tasks
Crontab.Add  'master_ftp_scan', '* * * * * /usr/bin/env ruby /echoware/code/schedule/master_ftp_scan.rb'	#Scans the FTP for new files to import
Crontab.Add  'master_jobber', '* * * * * /usr/bin/env ruby /echoware/code/schedule/master_jobber.rb'		#Runs jobs which are on the stack
Crontab.Add  'master_notifier', '* * * * * /usr/bin/env ruby /echoware/code/schedule/master_notifier.rb'	#Notifys admins of log issues or other items		
