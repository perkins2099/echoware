#!/usr/bin/env ruby
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'
require 'logger'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	how_many_seconds = 60

	#If I am running to long.  Add to list.  Email Admins
	tdate = Time.now - how_many_seconds

	switches = master[:switch].where(:swi_running=>1).and(:swi_last_start.identifier < tdate)

	if switches.count > 0
		message_string = "Switch Errors:\r\n\r\n"
	end

	switches.each do |tswitch|
		switch = Switch[tswitch[:swi_id]]
		message_string = message_string + switch.swi_value + " has been running since: " + switch.swi_last_start.to_s() + "\n\n" 
	end


	logs = master[:log].join(:log_message,:lom_id=>:log_lom_id).where(:log_date_created.identifier > tdate)
	if logs.count > 0 
		message_string = message_string + "\r\nLogging Errors: \r\n\r\n"
	end

	logs.each do |log|
		#log = Log[tlog[:log_id]]
		message_string = message_string + log[:lom_value] + " | " + log[:log_more] + " :" + log[:log_date_created].to_s() + "\r\n \r" 
	end



#	puts logs.count
#	puts message_string

	if message_string != "" and message_string != nil
	#puts message_string
		admins = master[:user].where(:usr_ust_id => 1)
		admins.each do |tadmin|
			admin = User[tadmin[:usr_id]]
			if admin.usr_email != nil and admin.usr_email != ""
				send_email(admin.usr_email, :body=>message_string, :subject=>"Long RunTime - Need attention")
			end
		end
	end



result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
file = File.open("/echoware/master_notifier.html", "w+")
printer.print(file)
file.close



