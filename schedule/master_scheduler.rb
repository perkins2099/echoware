#!/usr/bin/env ruby
require 'cronedit'
include CronEdit
require "/echoware/code/db.rb"
require "/echoware/code/class/master.rb"
require "rubygems"
require "sequel"
require 'ruby-prof'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	schedules = master[:export_schedule].where(:esc_sct_id => 1).and(:esc_scs_id => [1,3,2])	

	cur_time = Time.new

	schedules.each do |schedule|
	s = Export_schedule[schedule[:esc_id]]
		cron_paths = "/usr/bin/env"
		if s.esc_date_start <= cur_time
			if s.esc_date_end.nil? or s.esc_date_end > cur_time #Start the scheduling process for this scheduler 
				Crontab.Add  "#{s.esc_name}", "#{s.esc_cron_pattern} #{cron_paths} #{s.esc_action}"
#				puts "#{s.esc_name}", "#{s.esc_cron_pattern} #{cron_paths} #{s.esc_action}"
				s.active()
			elsif s.esc_date_end <= cur_time #Terminate the scheduling process for this scheduler
				Crontab.Remove  "#{s.esc_name}"
				s.terminate()
			else
				Log.create.logme_error(2,5,8,nil,s.sch_id,nil,"Scheduler didn't fall within expected ranges" ) #Level, Type, Message, Job, LinkType,Customer,More)
			end
		end

	end

result = RubyProf.stop
printer = RubyProf::GraphHtmlPrinter.new(result)
datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
file = File.open("/echoware/master_scheduler.html", "w+")
printer.print(file)
file.close
