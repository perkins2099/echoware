#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"

#------------------------------START Translation Head------------------------------------------------#
	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	job_id= ARGV[0] #Get the id of the Job
	job=Job[job_id]	#Job record stored

	customer = Customer[job.job_cus_id] #Get the customer record

	imports = master[:import].filter(:imp_bat_id => batch.bat_id)	#List of all imports in batch

	import = imports.first #Just get the 'first one' since MOST imports will only have one import file in a batch

	customer_db = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer.cus_db)
#------------------------------END Translation Head--------------------------------------------------#

#table_name= 'imp_' + import_record[:imp_value]


#------------------------------START Translation BODY------------------------------------------------#
	begin
		#Step 0-Get the list of our new folks
		new_data = customer_db[:employee].where(:ee_bai_id => batch_instance.bai_id).and(:ee_ees_id => 2)

		#Step 1-Do a lookup on all the employees and find their unique system ID.  If new, add to the table.  If not new, look up.  Assign accordingly.
		#1.Find those who are not in the list.
		have_id = customer_db[:employee_id].select(:eei_ssn)
		#2.Create a new ID for them
		need_id = customer_db[:employee].where(:ee_bai_id => batch_instance.bai_id).and(:ee_ees_id => 2).left_join(:employee_id, :eei_id => :ee_eei_id).where(:eei_id => nil).select(:ee_ssn)

		customer_db[:employee_id].insert([:eei_ssn, :eei_clock],
		need_id.select(:ee_ssn, :ee_clock).group(:ee_ssn,:ee_clock))

		#3.Assign ID to employee record
		new_data.join(:employee_id, :eei_ssn => :ee_ssn).update(:ee_eei_id => :eei_id)

		#Step 2-Check for duplicate records.  Log / Do Not mark as OK.
		duplicates = new_data.group(:ee_ssn).having{count(ee_ssn) > 1}

		if duplicates.count > 0
			Log.create.logme_error(2,3,4,job.job_id,nil,j.job_cus_id,duplicates.select_map(:ee_ssn) ) #Level, Type, Message, Job, LinkType,Customer,More)
		end

		#Step 3-Extra difficult translations / Updates

		#Step 4-MOVE ALL DATA INTO PRIMARY STORAGE TABLES (DO NOT MANIPULATE PRIMARY STORAGE TABLES)


		#Step N-Assign errors to emp records
		new_data.where(:ee_ssn => duplicates.select_map(:ee_ssn)).update(:ee_ees_id=>3)	#Duplication SSN's

		#Step N-Assign the employee record to completion
		new_data.update(:ee_ees_id=>1)
		
	rescue Exception => e
		Log.create.logme_error(3,3,5,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
	end

#------------------------------END Translation BODY--------------------------------------------------#



job.complete_job()	
