#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"
	require 'fileutils'

	job_id= ARGV[0] 				#Get the id of the Job
	job=Job[job_id]					#Job record stored
	customer = Customer[job.job_cus_id] 		#Get the customer record
	export = Export[job.job_link_id]		#Get the export record
	customer_db = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer.cus_db)

#Step 1 - Make sure there is a directory and clear out any old files

	path = $path_ftp + customer.cus_db + '/' + export.exp_value + '/'
	if File::directory?( path )
		FileUtils.rm Dir.glob(path+ '*')
	else
#		Dir::rmdir(path)
		Dir::mkdir(path)
		File.chmod(0777, path)   #Make sure it is writeable
	end

	job.job_file = path+export.get_file_name()
	job.save

#Step 3 - Construct the OUT FILE Statement to create the file / Create File
	sql = "SELECT 
	ee_id,ee_eei_id,ee_ssn,ee_clock,ee_number,ee_name_first,ee_name_last,ee_department,ee_address_street1,ee_address_street2,ee_address_city,ee_address_state,ee_address_zip

	  INTO OUTFILE '#{path+export.get_file_name()}'
	  FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY ''
	  LINES TERMINATED BY '\\n'
	  FROM #{customer.cus_db}.employee
	where ee_ssn IS NOT NULL;"
#	puts sql
	begin
		customer_db.run(sql)
		#customer_db[sql]
		#customer_db.execute(sql)

	rescue Exception => e
		Log.create.logme_error(3,2,7,job.job_id,job.job_link_id,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
		job.error()
		exit
	end

job.complete_job()	

	
