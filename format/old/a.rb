#!/usr/bin/ruby -w
#!/usr/bin/env ruby

#require "mysql2"
#require "rubygems"
#require "find"
require "/echoware/code/db.rb"
#require "/echoware/code/common.rb"
#require "/echoware/code/import/bulk_dump_to_mysql.rb"
require "rubygems"
require "active_record"
require 'ruby-prof'
#require "unprof"


RubyProf.start

ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :host => "localhost",
    :username => "root",
    :password => "root",
    :database => "master")


class Log < ActiveRecord::Base

end



logs = Log.find(:all)

logs.each do |tlog|
	puts tlog["name"]
end


#puts logs[1].name


#t = Log.where(:name=>"Second Log")
#puts %{#{t.name}}
#puts t[0].log_id

#tz = Log.find(2)
#puts tz[:name]


#tz = Log.new(:name=>"Test")
#puts tz[:name]
#puts tz[:log_id]
#tz.save
#puts tz[:log_id]

result = RubyProf.stop
printer = RubyProf::FlatPrinter.new(result)
printer.print(STDOUT,1)
