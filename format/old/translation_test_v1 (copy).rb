#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"
	require 'ruby-prof'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

#------------------------------START Translation Head------------------------------------------------#
	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	job_id= ARGV[0] #Get the id of the Job
	job=Job[job_id]	#Job record stored

	customer = Customer[job.job_cus_id] #Get the customer record

	batch_instance = Batch_instance[job.job_link_id]	#Get the batch instance record
	batch = Batch[batch_instance.bai_bat_id]	#Get the batch record

	imports = master[:import].filter(:imp_bat_id => batch.bat_id)	#List of all imports in batch

	import = imports.first #Just get the 'first one' since MOST imports will only have one import file in a batch

	customer_db = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer.cus_db)
#------------------------------END Translation Head--------------------------------------------------#

#table_name= 'imp_' + import_record[:imp_value]


#------------------------------START Translation BODY------------------------------------------------#
	begin
		#Step 0-Get the list of our new folks
puts 't1'
		new_data = customer_db[:employee].where(:ee_bai_id => batch_instance.bai_id).and(:ee_ees_id => 2)
puts 't2'
		#Step 1-Do a lookup on all the employees and find their unique system ID.  If new, add to the table.  If not new, look up.  Assign accordingly.
		#1.Find those who are not in the list.
		have_id = customer_db[:employee_id].select(:eei_ssn)
puts 't3'
		#2.Create a new ID for them
puts		customer_db[:employee_id].insert([:eei_ssn, :eei_clock],
		new_data.select(:ee_ssn, :ee_clock).exclude(:ee_ssn => have_id).group(:ee_ssn,:ee_clock)).sql
exit
		#customer[:employee].select(:ee_ssn, :ee_clock).where(:ee_bai_id => batch_instance_record.bai_id).and(:ee_ees_id => 2).exclude(:ee_ssn => have_id).group(:ee_ssn,:ee_clock))
puts 't4'
		#3.Assign ID to employee record
		new_data.join(:employee_id, :eei_ssn => :ee_ssn).update(:ee_eei_id => :eei_id)
puts 't5'

		#Step 2-Check for duplicate records.  Log / Do Not mark as OK.
		duplicates = new_data.group(:ee_ssn).having{count(ee_ssn) > 1}
puts 't6'
		if duplicates.count > 0
			Log.create.logme_error(2,3,4,job.job_id,nil,j.job_cus_id,duplicates.select_map(:ee_ssn) ) #Level, Type, Message, Job, LinkType,Customer,More)
		end

		#Step 3-Extra difficult translations / Updates

		#Step 4-


		#Step N-Assign errors to emp records
		new_data.where(:ee_ssn => duplicates.select_map(:ee_ssn)).update(:ee_ees_id=>3)	#Duplication SSN's
puts 't7'
		#Step N-Assign the employee record to completion
		new_data.update(:ee_ees_id=>1)
puts 't8'
		#Step N-Assign the batch_instance record to completion
		2# Complete
		3# Error
		batch_instance.complete()
	rescue Exception => e
		Log.create.logme_error(3,3,5,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
		batch_instance.error()
	end

#------------------------------END Translation BODY--------------------------------------------------#


#j.update_job_status_success()

result = RubyProf.stop
#printer = RubyProf::GraphHtmlPrinter.new(result)
#datetime = Time.now.strftime("%Y%m%d%H%M%S")
#file = File.open("/echoware/[file]_#{datetime}.html", "w+")
#file = File.open("/echoware/stuff.html", "w+")
#printer.print(file)
#file.close

