#!/usr/bin/ruby -w

def bulk_dump_to_mysql(cus_db, imp_directory, imp_table_name, raw_file_name)
	require "mysql2"
	require "rubygems"
	require "logger"

	#Adam Libraries
	#require "/echoware/code/import/import.rb"
	require "/echoware/code/db.rb"

	customer = Mysql2::Client.new(:host => $host, :username => $username, :password => $password, :database =>cus_db )
	begin	#General Catchall for rescue.  Just because one job fails doesn't mean they all should.

#Create BATCH
#If there is an ERROR on import.  Mark the batch as bad.  If a bad batch DO NOTHING WITH.  Throw error.  STop Exports?  Exports only run on good batch.
#Insert Data into table
#Check Footer for counts

		begin
			sql = "DROP TABLE IF EXISTS " + imp_table_name
			customer.query(sql)
		rescue
			puts "Log import error2 goto end"
			puts sql
		end

		begin
			sql = "CREATE TABLE " + imp_table_name + " (  imp_id INT NOT NULL AUTO_INCREMENT ,  imp_tdata VARCHAR(8000) NULL ,  PRIMARY KEY (`imp_id`) )ENGINE = InnoDB;"
			customer.query(sql)
		rescue
			puts "Log error 3 (goto end)"
			puts sql
		end

		begin
			sql = "INSERT INTO " + imp_table_name + "(imp_tdata) VALUES "
			File.open(raw_file_name, 'rb') do |file_handle|
				file_handle.each do |server|
					sql = sql+ ("('" + server.strip + "'),")
				end
			end
		rescue
			puts "log erro4 (goto end"
		end


		begin	# Insert into the raw database tables
			sql = sql.chomp(",")
			customer.query(sql)
		rescue
			puts "customer2.query(5 goto end"
		end


#Query the file and make sure the counts match the new imported information
#	raw_count = File.foreach(full_file_path).inject(0) {|c, line| c+1}
#	sql = "select count(*) as cc from " + table_name
#	results = customer2.query(sql)
#	db_count = ""

#	results.each do |result|
#		db_count = result["cc"]
#	end

#	puts raw_count
#	puts db_count
#	if raw_count != db_count
#		puts "Error"
#		sql = "INSERT INTO " + table_name + "(imp_tdata) SELECT 'ERROR : Mismatch row counts.  DB_COUNT: #{db_count}  RAW_COUNT: #{raw_count}'"
#		results = customer2.query(sql)

#	end	


#puts results

	#file.close

	#move_file_to_storage(db_name, import_name, file_name)

	rescue
		puts "General error, err logs"
	end
	
#	file.close
	customer.close
end

