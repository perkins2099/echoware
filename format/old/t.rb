#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"
	require 'ruby-prof'

RubyProf.measure_mode = RubyProf::WALL_TIME
RubyProf.start

#------------------------------START Translation Head------------------------------------------------#
	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database, :timeout =>1)

	job_id= ARGV[0] #Get the id of the Job
	job=Job[job_id]	#Job record stored

	customer = Customer[job.job_cus_id] #Get the customer record

	batch_instance = Batch_instance[job.job_link_id]	#Get the batch instance record
	batch = Batch[batch_instance.bai_bat_id]	#Get the batch record

	imports = master[:import].filter(:imp_bat_id => batch.bat_id)	#List of all imports in batch

	import = imports.first #Just get the 'first one' since MOST imports will only have one import file in a batch

	customer_db = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer.cus_db, :timeout=> 1)


	new_data = customer_db[:employee].where(:ee_bai_id => batch_instance.bai_id).and(:ee_ees_id => 2)

	need_id = customer_db[:employee].where(:ee_bai_id => batch_instance.bai_id).and(:ee_ees_id => 2).left_join(:employee_id, :eei_id => :ee_eei_id).where(:eei_id => nil).select(:ee_ssn)

	puts "pre"
	customer_db[:employee_id].insert([:eei_ssn, :eei_clock],
	need_id.select(:ee_ssn, :ee_clock).group(:ee_ssn,:ee_clock))

	puts "next"
	new_data.join(:employee_id, :eei_ssn => :ee_ssn)

	puts a		#Assign ID to emp 

	puts "finish"
