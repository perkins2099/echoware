#!/usr/bin/ruby -w
#!/usr/bin/env ruby

require "mysql2"
require "rubygems"
require "find"
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/import/bulk_dump_to_mysql.rb"
#require 'logger'
require 'ruby-prof'
#require 'unprof'
#require "active_record"

#Adam Library
#$LOAD_PATH << './lib'
#require "db.rb"

RubyProf.start

	begin
		master = Mysql2::Client.new(:host => $host, :username => $username, :password => $password, :database =>$database ) 
		results = master.query("select * from v_get_imports")
	rescue 
		puts "Rescue me add to log file about this connection failling for import"
	end

results.each do |result|

	cus_db = result["cus_db"]
	imp_directory = result["imp_directory"]
	fvs_file_name = result["fvs_file_name"]
	imp_table_name = result["imp_table_name"]	

	path ="/echoware/ftp/#{cus_db}/#{imp_directory}"

	#If this directory exists
	if File::directory?( path )
		Find.find(path) do |file|
			#If there is a file inside here
			if File::file?( file )
				case fvs_file_name
				when "bulk_dump_to_mysql.rb"
					batch=bulk_dump_to_mysql(cus_db,imp_directory,imp_table_name,fvs_file_name)
					#log_import(batch)
					#move_file_to_storage(customer, import, file.split("/")[-1])
				else
					puts "Error here add to log file"
				end

			end

		end
	end
end



result = RubyProf.stop
printer = RubyProf::FlatPrinter.new(result)
printer.print(STDOUT)
