require 'fileutils'


def move_file_to_storage(client, vendor, file)

	from_file = "/echoware/ftp/#{client}/#{vendor}/#{file}"
	time = File.ctime(from_file)
	append = time.strftime("%Y%m%d_%H%M%S")
	to_file = "/echoware/storage/#{client}/#{vendor}/#{append}_#{file}" 

	#puts to_file
	#from_dir = "/echoware/ftp/#{client}/#{vendor}/"
	to_dir = "/echoware/storage/#{client}/#{vendor}/"

	#If No directory..create it
	if File.directory?(to_dir) == false
		FileUtils.mkdir_p to_dir
	end

	begin	
		FileUtils.copy from_file, to_file
	rescue
		$LOG.error("Error moving files.  From: #{from_File}   To:#{to_file}")  
	end

end



