#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

	job_id= ARGV[0] #Get the id of the Job
	job=Job[job_id]	#Job record stored

	customer_record = Customer[job.job_cus_id] #Get the customer record
	import_record = Import[job.job_link_id]	#Get the import record

	customer = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer_record.cus_db)

#------------------------------START Logic to Load data into temp table------------------------------------------------#

	customer.create_table! 'imp_' + import_record.imp_value do	#Drop / recreate the table based on the 'new' data coming in for this import
		primary_key :imp_id
		Integer :bai_id
		String :type
		String :f1
		String :f2
		String :f3
		String :f4		
		String :f5
		String :f6		
		String :f7
		String :f8
		String :f9
		String :f10
		String :f11
		String :f12
		String :f13
		String :f14
		String :f15
		String :f16
		String :f17
		String :f18
		String :f19
		String :f20	
	end 

#		sql="LOAD DATA LOCAL INFILE '#{job.job_file}' INTO TABLE imp_#{import_record.imp_value}  FIELDS TERMINATED BY '|' (type,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20)"
		sql="LOAD DATA LOCAL INFILE '#{job.job_file}' INTO TABLE imp_#{import_record.imp_value}  FIELDS TERMINATED BY '|' (type,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20)"
puts sql
	begin
		customer.execute(sql)	
	rescue
		new_log = Log.create(:log_lol_id => 3, :log_date_created => Time.now.to_s(),:log_job_id => job_id,:log_lot_id => 1,:log_lom_id =>1,:log_imp_id =>import_record.imp_id, :log_more => sql, :log_cus_id => job.job_cus_id)
			
		job.job_jos_id = 4 #Done with errors
		job.job_date_completed = Time.now.to_s()
		job.save
		exit
	end

	
	table_name =  ('imp_' + import_record.imp_value)

	trailer_count = customer[table_name.to_sym].filter(:type => 'T').count
	trailer_record_count = customer[table_name.to_sym].filter(:type => 'T').first
	total_records = customer[table_name.to_sym].filter(:type => 'D' ).count
	trc = trailer_record_count[:f1]

#	puts "trs"
#	puts "Count of records with D:" + total_records.to_s()
#	puts "Number of trailer records:" + trailer_count.to_s()
#	puts "F1 record in trailer:" + trc.to_s()


	if trailer_count != 1 or total_records.to_i() != trc.to_i()

		new_log = Log.create(:log_lol_id => 3, :log_date_created => Time.now.to_s(),:log_job_id => job_id,:log_lot_id => 1,:log_lom_id =>2,:log_imp_id =>import_record.imp_id, :log_more => 'Trailer Record Count: '+ trc.to_s(), :log_cus_id => job.job_cus_id)
		job.job_jos_id = 4 #Done with errors
		job.job_date_completed = Time.now.to_s()
		job.save
		exit
	end
#------------------------------END Logic to Load data into temp table--------------------------------------------------#



#------------------------------Job Completed---------------------------------------------------------------------------#


