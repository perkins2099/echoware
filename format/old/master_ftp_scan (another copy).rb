#!/usr/bin/ruby 
#!/usr/bin/env ruby

require "rubygems"
require "find"
require "sequel"
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/import/bulk_dump_to_mysql.rb"
require "/echoware/code/import/import_test_v2.rb"
require "/echoware/code/class/master.rb"
#require 'logger'
require 'ruby-prof'
#require 'unprof'
#require "active_record"

#Adam Library
#$LOAD_PATH << './lib'
#require "db.rb"

RubyProf.start

#	begin
#		master = Mysql2::Client.new(:host => $host, :username => $username, :password => $password, :database =>$database ) 
#		results = master.query("select * from v_get_imports")
#	rescue 
#		puts "Rescue me add to log file about this connection failling for import"
#	end
	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

#imports=master[:import].join(:format, :for_id => :imp_for_id).join(:format_version, :fvs_for_id => :for_id).join(:customer, :cus_id => :import__imp_cus_id).group(
imports = master["select * from v_get_imports"]
#puts imports

imports.each do |import|
	puts import[:cus_db]
	path =$path_ftp + "#{import[:cus_db]}/#{import[:imp_directory]}"

	#If this directory exists
	if File::directory?( path )
	
		Find.find(path) do |raw_file_name|
#			puts raw_file_name
			#If there is a file inside here
			if File::file?( raw_file_name )
				case import[:fvs_file_name]			
				when "bulk_dump_to_mysql.rb"
				#puts "hey" + raw_file_name
					bulk_dump_to_mysql(import[:cus_db],import[:imp_directory],import[:imp_table_name],raw_file_name)
					#log_import(batch)
					#move_file_to_storage(customer, import, file.split("/")[-1])
				when "import_test_v1.rb"
					import_test_v1(import[:cus_db],import[:imp_directory],import[:imp_table_name],raw_file_name,1)
				else
					puts "Error here add to log file"
				end
			end
		end
	end
end

result = RubyProf.stop
#printer = RubyProf::FlatPrinter.new(result)
#printer.print(STDOUT)
