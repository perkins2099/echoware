#!/usr/bin/ruby 

def import_test_v1(cus_db, imp_directory, imp_table_name, raw_file_name, imp_id)

	require "rubygems"
	require "sequel"
	require "/echoware/code/db.rb"
	require "/echoware/code/class/event.rb"
	require "/echoware/code/class/client_class.rb"
	require "time"
#	require "faster_csv"
	require "csv"

#	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)
#	customer = Sequel.connect(:adapter=>'mysql2', :host=>'localhost', :user=>'root',:password=>'root', :database=>cus_db)

#	begin	#General Catchall for rescue.  Just because one job fails doesn't mean they all should.

#Create Event - assign to imported data

#If there is an ERROR on import.  Mark the batch as bad.  If a bad batch DO NOTHING WITH.  Throw error.  STop Exports?  Exports only run on good batch.
#Insert Data into table
#Check Footer for counts

	#Event Create for this import
	event = Event.create()
	event.eve_evt_id = 1	#Import
	event.eve_imp_id = imp_id
	event.save

	eve_id = event.eve_id

	customer = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>cus_db)

	employees = customer[:employee]

	CSV.foreach(raw_file_name, :headers => true, :quote_char => '"', :col_sep =>'|', :row_sep =>:auto) do |row|
		#Make this transactional and roll back if checks aren't compelted
		#Pull off the LAST row for counts.  Compare counts in this scripts.  Do not load if counts do not match
		#Update Event if successful or failure.
		#Period notifications of event stack???
		#Load Data INFILE MySQL
	employees.insert(:ee_name_first => row['f1'], 
			:ee_name_last => row['f2'], 
			:ee_name_middle => row['f3'],
			:ee_name_Suffix => row['f4'],
			:ee_name_prefix => row['f5'],
			:ee_clock => row['f6'],
			:ee_number => row['f7'],
			:ee_ssn => row['f8'],
			:ee_address_street1 => row['f9'],
			:ee_address_street2 => row['f10'],
			:ee_address_city => row['f11'],
			:ee_address_state => row['f12'],
			:ee_address_zip =>  row['f13'],
			:ee_eve_id => eve_id )
	end

	event.eve_date_finished = Time.now.to_s()
	event.save

	
end

