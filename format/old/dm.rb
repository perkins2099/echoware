#!/usr/bin/ruby -w
#!/usr/bin/env ruby

require "mysql2"
require "rubygems"
require "find"
require "/echoware/code/db.rb"
require "/echoware/code/common.rb"
require "/echoware/code/import/bulk_dump_to_mysql.rb"
#require 'logger'
require 'ruby-prof'
require 'sequel'
#require 'unprof'
#require "active_record"

RubyProf.start

	DataMapper.setup(:default, 'mysql://user:rootlocalhost/master')


#results.each do |result|
#	puts result["name"]
#end

result = RubyProf.stop
printer = RubyProf::FlatPrinter.new(result)
printer.print(STDOUT,1)
