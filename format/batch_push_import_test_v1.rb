#!/usr/bin/ruby 

	require "rubygems"
	require "sequel"
	require "time"
	require "/echoware/code/db.rb"
	require "/echoware/code/common.rb"
	require "/echoware/code/class/master.rb"


	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)
	timestamp = Time.now.to_s()

	job_id= ARGV[0] #Get the id of the Job
	job=Job[job_id]	#Job record stored

	customer = Customer[job.job_cus_id] #Get the customer record
	import = Import[job.job_link_id]	#Get the import record

	customer_db = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>customer.cus_db)

#------------------------------START Logic to Load data-------------------------------------------------------------#

#------------------------------NOTE For new imports upload into a separate table -- NEver upload into a user table-----------------------------------------#


#ENCLOSED BY '"'
	table_name = 'imp_' + import.imp_value

	customer_db.create_table! table_name do	#Drop / recreate the table based on the 'new' data coming in for this import
		primary_key :imp_id
		Integer :ee_job_id
		Integer :ee_eei_id 
		String :ee_name_first
		String :ee_name_last
		String :ee_ssn
		String :ee_address_street1
		String :ee_address_street2
		String :ee_address_city
		String :ee_address_state
		String :ee_address_zip
		String :ee_department
		String :ee_date_first_hire
		String :ee_clock
	end 

	sql = "LOAD DATA LOCAL INFILE '#{job.job_file}' 
	INTO TABLE #{table_name} 
	FIELDS TERMINATED BY '|' 
	LINES STARTING BY 'DET*'
	(@ignore,ee_name_first,ee_name_last,ee_ssn,ee_address_street1,ee_address_street2,ee_address_city,ee_address_state,ee_address_zip,ee_department,@ee_date_first_hire,ee_clock)
	SET ee_job_id = #{job.job_id},ee_date_first_hire = STR_TO_DATE(@ee_date_first_hire, '%m/%d/%Y'), ee_ssn = replace(ee_ssn,'-','');"

	begin
		customer_db.execute(sql)	
	rescue Exception => e
		Log.create.logme_error(3,1,1,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)		
		job.error()
		exit
	end

#------------------------------END Logic to Load data-------------------------------------------------------------#

#------------------------------START Logic to Load trailer data-------------------------------------------------------------#	
	sql = "LOAD DATA LOCAL INFILE '#{job.job_file}' 
	INTO TABLE trailer_record
	FIELDS TERMINATED BY '|' 
	LINES STARTING BY 'TRA*'
	(@symbol,@count)
	SET tra_job_id = #{job.job_id}, tra_record_type = 'DetailRecords', tra_count = @count, tra_date_created = '#{timestamp}';"

	begin
		customer_db.execute(sql)	
	rescue Exception => e
		Log.create.logme_error(3,1,3,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)		
		job.error()
		exit
	end

	trailer = customer_db[:trailer_record].where(:tra_job_id=>job.job_id).and(:tra_record_type => 'DetailRecords').and(:tra_date_created=> timestamp).first
	count_rows = customer_db[table_name.to_sym].filter(:ee_job_id => job.job_id).count
	if count_rows != trailer[:tra_count]
		Log.create.logme_error(3,1,2,job.job_id,nil,job.job_cus_id,'Trailer Record Count: '+ trailer[:tra_count].to_s() + ' | Row count: ' + count_rows.to_s() ) #Level, Type, Message, Job, LinkType,Customer,More)
		job.error()
		exit
	end
#------------------------------END Logic to Load trailer data-------------------------------------------------------------#
#------------------------------START Logic to Move around data-------------------------------------------------------------#
	begin

		new_data = customer_db[table_name.to_sym]

		#Step 1-Do a lookup on all the employees and find their unique system ID.  If new, add to the table.  If not new, look up.  Assign accordingly.
		need_id = new_data.left_join(:employee_id, :eei_id => :ee_eei_id).where(:eei_id => nil).select(:ee_ssn)

		customer_db[:employee_id].insert([:eei_ssn, :eei_clock],need_id.select(:ee_ssn, :ee_clock).group(:ee_ssn,:ee_clock))

		#3.Assign ID to employee record
		new_data.join(:employee_id, :eei_ssn => :ee_ssn).update(:ee_eei_id => :eei_id)

		#Step 2-Check for duplicate records.  Log / Do Not mark as OK.
		duplicates = new_data.group(:ee_ssn).having{count(ee_ssn) > 1}

		if duplicates.count > 0
			Log.create.logme_error(2,3,4,job.job_id,nil,job.job_cus_id,duplicates.select_map(:ee_ssn) ) #Level, Type, Message, Job, LinkType,Customer,More)
		end
		

		#Step 4-MOVE ALL DATA INTO PRIMARY STORAGE TABLES (DO NOT MANIPULATE PRIMARY STORAGE TABLES)
		customer_db[:employee].insert([:ee_ssn, :ee_clock, :ee_job_id,:ee_eei_id, :ee_name_first, :ee_name_last, :ee_address_city, :ee_address_street1, :ee_address_street2, :ee_address_state, :ee_address_zip, :ee_department, :ee_date_first_hire],new_data.select(:ee_ssn, :ee_clock, :ee_job_id,:ee_eei_id, :ee_name_first, :ee_name_last, :ee_address_city, :ee_address_street1, :ee_address_street2, :ee_address_state, :ee_address_zip, :ee_department, :ee_date_first_hire))

		#Step N-Assign errors to emp records
		#new_data.where(:ee_ssn => duplicates.select_map(:ee_ssn)).update(:ee_ees_id=>3)	#Duplication SSN's

		
	rescue Exception => e
		Log.create.logme_error(3,3,5,job.job_id,nil,job.job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)

	end

#------------------------------Job Completed---------------------------------------------------------------------------#
#If successful return true
job.complete_job()

