	require "rubygems"
	require "sequel"
	require "/echoware/code/db.rb"

master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)


class Customer < Sequel::Model(:customer)

end

class Import < Sequel::Model(:import)
end

class Export < Sequel::Model(:export)

	def get_file_name()
		the_date = Time.now
	if the_date.month < 10 
		full_month = '0' + the_date.month.to_s() 
	else 
		full_month = the_date.month.to_s() 
	end
	if the_date.day < 10 
		full_day = '0' + the_date.day.to_s() 
	else 
		full_day = the_date.day.to_s() 
	end

	file_name = exp_file_name
	file_name = file_name.gsub("%yyyy", the_date.year.to_s())	#4 digit year
	file_name = file_name.gsub("%cc", the_date.year.to_s()[0..1])	#Century
	file_name = file_name.gsub("%yy", the_date.year.to_s()[2..4])	#Year
	file_name = file_name.gsub("%mm", full_month)	#Enforce leading 0
	file_name = file_name.gsub("%dd", full_day)		#Enforce Leading 0
	file_name = file_name.gsub("%m", the_date.month.to_s())		#Do not enforce leading 0
	file_name = file_name.gsub("%d", the_date.day.to_s())		#Do not enforce leading 0
		
	file_name	
	end
end


class Format_version < Sequel::Model(:format_version)

end

class Format < Sequel::Model(:format)
	def get_max_fvs_id(master)
		format_versions = master[:format_version].filter(:fvs_for_id => self.for_id).order(:fvs_date_start.desc)
		format_versions.first[:fvs_id]
	end
	
end


class Job < Sequel::Model(:job)
	def newjob(in_job_jot_id,in_job_for_id,in_job_cus_id,in_job_link_id,in_file_name) #Type,Format,Customer,LinkID,Raw_File_Name

		self.job_link_id = in_job_link_id
		self.job_jos_id = 1
		self.job_jot_id = in_job_jot_id
		self.job_date_created = Time.now.to_s()
		self.job_cus_id = in_job_cus_id
		self.job_for_id = in_job_for_id
		self.job_file = in_file_name
		self.save
	end

	def complete_job() #Will complete any job that is put on the job queue.  Handles imports, exports, and more into one spot.
		require "/echoware/code/db.rb"
		require "sequel"
		master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)
		if self.export?	
		elsif self.translation?	#If translation this will mark it
				#Batch_instance[self.job_bai_id].all_jobs_complete()
			#1. Mark Job as complete
			#3. Schedule Dependent Jobs
		elsif self.import?
			#Check to see if jobs are waiting to run on this.. If yes schedule a new job.. If no.. nothing.  Hurray its done.
			master[:export_schedule].join(:export, :exp_id => :esc_exp_id).where(:esc_sct_id=>2).and(:esc_imp_id=>self.job_link_id).and(:esc_scs_id => [1,3]).each do |export|	
				Job.create().newjob(2,export[:exp_for_id],export[:exp_cus_id],export[:exp_id],nil)#Type,Format,Customer,LinkID,Raw_File_Name
			end
		end	
		self.success
		rescue Exception => e
			Log.create.logme_error(3,4,13,job_id,nil,job_cus_id,e.message + ' | ' + e.backtrace.inspect ) #Level, Type, Message, Job, LinkType,Customer,More)
			self.error()
	end

	def success?
		if self.job_jos_id == 3 
			true
		else
			false
		end
	end
	def running?
		if self.job_jos_id == 2
			true
		else
			false
		end
	end
	def error?
		if self.job_jos_id == 4
			true
		else
			false
		end
	end
	def queued?
		if self.job_jos_id == 1
			true
		else
			false
		end
	end
	def translation?
		if self.job_jot_id == 3
			true
		else
			false
		end
	end
	def import?
		if self.job_jot_id == 1
			true
		else
			false
		end
	end
	def export?
		if self.job_jot_id == 2
			true
		else
			false
		end
	end

	def queued
		self.job_jos_id = 1
		self.save
	end
	def running
		self.job_jos_id = 2
		self.job_date_started = Time.now.to_s()
		self.save
	end
	def success
		self.job_jos_id = 3
		self.job_date_completed = Time.now.to_s()
		self.save
	end
	def error
		self.job_jos_id = 4
		self.job_date_completed = Time.now.to_s()
		self.save
	end

end

def move_file_to_storage(in_old_name,in_new_name,in_cus_id, master)
		require 'fileutils'

		customer = master[:customer].where(:cus_id => in_cus_id).first

		dir = $path_storage + customer[:cus_db]
	
		#If No directory..create it
		if File.directory?(dir) == false
			FileUtils.mkdir_p dir
		end

		begin	
			FileUtils.move in_old_name, in_new_name
		rescue
			Log.create.logme_error(3,8,15,job_id,nil,nil,job_file + '|' + dir + '|' + new_file_name) #Level, Type, Message, Job, LinkType,Customer,More)
			#Log.create.logme_error(3,8,15,job_id,nil,nil,"error") #Level, Type, Message, Job, LinkType,Customer,More)
			puts "Error in move file to storage" 
		end
end

def get_import_file_name(in_raw_file_name,in_import)
	require "rubygems"
	require "find"
	require "sequel"
	require "/echoware/code/db.rb"
	master = Sequel.connect(:adapter=>'mysql2', :host=>$host, :user=>$username,:password=>$password, :database=>$database)

		import = master[:import].where(:imp_id => in_import[:imp_id]).first

		customer = master[:customer].where(:cus_id => in_import[:imp_cus_id]).first

		time = File.ctime(in_raw_file_name)
		append = time.strftime("%Y%m%d_%H%M%S")
		dir = $path_storage + customer[:cus_db] + "/"
		new_file_name = "generic.txt"

		new_file_name = import[:imp_value] + ".txt"
		new_file_name = append + "_" + new_file_name

		dir + new_file_name	
end


class Log < Sequel::Model(:log)
	def logme_error(in_lol_id,in_log_type,in_log_message,in_job_id,in_log_link, in_cus_id, in_log_more)
		self.log_lol_id = in_lol_id
		self.log_date_created = Time.now.to_s()
		self.log_job_id = in_job_id
		self.log_lot_id = in_log_type
		self.log_lom_id = in_log_message
		self.log_cus_id = in_cus_id
		self.log_link_id = in_log_link
		self.log_more = in_log_more

		self.save
	end
end


class Export_schedule < Sequel::Model(:export_schedule)

	def active
		self.esc_scs_id = 1
		self.esc_date_modified = Time.now.to_s()
		self.save
	end

	def terminate
		self.esc_scs_id = 2
		self.esc_date_modified = Time.now.to_s()
		self.save
	end
end

class Switch < Sequel::Model(:switch)
	def start()
		self.swi_running = 1
		self.swi_last_start = Time.now
		self.swi_last_end = nil
		self.save
	end
	def end()
		self.swi_running = 0
		self.swi_last_end = Time.now
		self.save
	end
end

def ok_date(in_date)
	if in_date.nil? || in_date > Time.now
		true
	else
		false
	end
end

class User < Sequel::Model(:user)
end



def send_email(to,opts={})
require 'net/smtp'

  opts[:server]      ||= 'smtp.gmail.com'
  opts[:from]        ||= 'EchoSys'
  opts[:from_alias]  ||= 'EchoSys'
  opts[:subject]     ||= "EchoSys Message"
  opts[:body]        ||= "Important stuff!"

  msg = <<END_OF_MESSAGE
From: #{opts[:from_alias]} <#{opts[:from]}>
To: <#{to}>
Subject: #{opts[:subject]}

#{opts[:body]}
END_OF_MESSAGE

	smtp = Net::SMTP.new 'smtp.gmail.com', 587
	smtp.enable_starttls()

	smtp.start(Socket.gethostname,'echowarenow@gmail.com','echoware1',:login) do |smtp|

	    smtp.send_message msg, opts[:from], to
	smtp.finish
  end
end


