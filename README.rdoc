Created: 02/01/2012
Author: Adam Perkins

EchoWare is a system which takes any flat file as input and turns maps / transforms it into the desired output.  EchoWare is more about the framework than the format as customer formats can be implemented at any time for any reason.

This program was originally intended to be a system in the HR / Payroll / Benefits world to facilitate the movement of data between payroll companies and 3rd party vendors.  Eventually it would have an API and have tight integrations.  

The project is shelved for the moment as I work on other projects.

This code is before discovering git and version control.  It is also my first stab at ruby scripting.