CREATE TRIGGER batch_time_stamp BEFORE INSERT ON batch
FOR EACH ROW
SET NEW.bat_date_created = NOW();
