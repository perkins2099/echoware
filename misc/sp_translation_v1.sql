DROP PROCEDURE IF EXISTS tra_translation_v1 ;

DELIMITER //
 CREATE PROCEDURE tra_translation_v1()
/********************************************************************************************
 
This procedure will take ficticious data from a flat file and order it into employee table

Change Log:
03/25/2012 : AAP : Created
*********************************************************************************************/


    BEGIN
       select * from employee;
    END //
 DELIMITER ;


call tra_translation_v1()