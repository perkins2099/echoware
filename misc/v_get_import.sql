use master;
drop view v_get_imports;
create view master.v_get_imports as
select * 
from import
inner join format on for_id = imp_for_id
inner join customer on imp_cus_id = cus_id
inner join format_version as fvs on fvs_for_id = for_id and fvs_date_start = (select max(fvs2.fvs_date_start) from format_version as fvs2 where fvs2.fvs_for_id = fvs.fvs_for_id and (fvs2.fvs_date_end is null or fvs2.fvs_date_end > curdate()));



