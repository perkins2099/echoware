use master;
delete from export_schedule;
delete from export;
delete from import;
delete from customer;
delete from batch;
delete from batch_instance;
delete from job;
delete from log;
#delete from export_schedule;
delete from format;
delete from format_version;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES 
(1,'bills','Bills key shop','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(2,'bobs','Bobs Copy shop','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(3,'carol','carols copy shop','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(4,'buffa','buffalo wild wings','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(5,'anyti','anytime fitness','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(6,'targe','target','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(7,'walgr','walgreens','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(8,'dot','department of transportation','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(9,'ihous','The Information House','2012-03-01 00:00:01',null,NOW(),NOW(),1),
(10,'lucky','Luckys irish pub and grill','2012-03-01 00:00:01',null,NOW(),NOW(),1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `format` WRITE;
/*!40000 ALTER TABLE `format` DISABLE KEYS */;
INSERT INTO `format` VALUES 
(1,'Import test',1,'This is the import test',NOW(),'2011-01-01',null,now(),1),
(2,'Export test',2,'THis is the export Test',NOW(),'2011-01-01',null,now(),1),
(3,'Translate',3,'This is the translate test',NOW(),'2011-01-01',null,now(),1);
/*!40000 ALTER TABLE `format` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `format_version` WRITE;
/*!40000 ALTER TABLE `format_version` DISABLE KEYS */;
INSERT INTO `format_version` VALUES 
(1,1,'2011-01-01','import_test_v4.rb',1,null,null,null,null),
(2,2,'2011-01-01','export_test_v1.rb',1,null,null,null,null),
(3,3,'2011-01-01','translation_test_v1.rb',1,null,null,null,null),
(4,4,'2011-01-01','batch_push_import_test_v1.rb',1,null,null,null,null);

/*!40000 ALTER TABLE `format_version` ENABLE KEYS */;
UNLOCK TABLES;



LOCK TABLES `batch` WRITE;
/*!40000 ALTER TABLE `batch` DISABLE KEYS */;
INSERT INTO `batch` VALUES 
(1,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(2,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(3,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(4,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(5,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(6,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(7,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(8,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(9,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1),
(10,3,NOW(),NOW(),1,'2012-03-01 00:00:01',null,168,1);
/*!40000 ALTER TABLE `batch` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `import` WRITE;
/*!40000 ALTER TABLE `import` DISABLE KEYS */;
INSERT INTO `import` VALUES 
(1,1,1,1,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
#(2,1,1,1,'2011-01-01',NOW(),NOW(),1,null,'Adp Import','adp'),
#(3,1,1,1,'2011-01-01',NOW(),NOW(),1,null,'Ceridian Import','ceridian'),
(4,2,1,2,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(5,3,1,3,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(6,4,1,4,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(7,5,1,5,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(8,6,1,6,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(9,7,1,7,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(10,8,1,8,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(11,9,1,9,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex'),
(12,10,1,10,'2011-01-01',NOW(),NOW(),1,null,'Paychex Import','paychex');
/*!40000 ALTER TABLE `import` ENABLE KEYS */;
UNLOCK TABLES;



LOCK TABLES `export` WRITE;
/*!40000 ALTER TABLE `export` DISABLE KEYS */;
INSERT INTO `export` VALUES 
(1,1,2,'2011-01-01',null,now(),now(),1,'Blue Cross Blue Shield Export','bcbs','bcbs%yyyy-%mm-%dd.txt',null),
(2,1,2,'2011-01-01',null,now(),now(),1,'United Health Care','uhc','uhc%yyyy-%mm-%dd.txt',null),
(3,1,2,'2011-01-01',null,now(),now(),1,'Delta Dental','delta','delta_dental_%yyyy-%mm-%dd.txt',null),
(4,1,2,'2011-01-01',null,now(),now(),1,'bSwift Export','bswift','bwsift_%yyyy-%mm-%dd.txt',null),
(5,1,2,'2011-01-01',null,now(),now(),1,'General Ledger Export','General Ledger','%dd_general_ledger%yy-%mm-.txt',null),
(6,1,2,'2011-01-01',null,now(),now(),1,'EyeMed','eyemed','eye_med_%yyyy-%mm-%dd.txt',null),
(7,1,2,'2011-01-01',null,now(),now(),1,'Optical Care','optical','optical_care_%yyyy-%mm-%dd.txt',null),
(8,1,2,'2011-01-01',null,now(),now(),1,'WPS Health Insurance','WPS','WPS_%yyyy-%mm-%dd.txt',null),
(9,1,2,'2011-01-01',null,now(),now(),1,'Dean Care','dean','dean_care%yyyy-%mm-%dd.txt',null),
(10,1,2,'2011-01-01',null,now(),now(),1,'FSA Express','fsa_express','fsa_express_%yyyy-%mm-%dd.txt',null),
(11,2,2,'2011-01-01',null,now(),now(),1,'Blue Cross Blue Shield Export','bcbs','bcbs%yyyy-%mm-%dd.txt',null),
(12,2,2,'2011-01-01',null,now(),now(),1,'United Health Care','uhc','uhc%yyyy-%mm-%dd.txt',null),
(13,2,2,'2011-01-01',null,now(),now(),1,'Delta Dental','delta','delta_dental_%yyyy-%mm-%dd.txt',null),
(14,2,2,'2011-01-01',null,now(),now(),1,'bSwift Export','bswift','bwsift_%yyyy-%mm-%dd.txt',null),
(15,3,2,'2011-01-01',null,now(),now(),1,'General Ledger Export','General Ledger','%dd_general_ledger%yy-%mm-.txt',null),
(16,3,2,'2011-01-01',null,now(),now(),1,'EyeMed','eyemed','eye_med_%yyyy-%mm-%dd.txt',null),
(17,4,2,'2011-01-01',null,now(),now(),1,'Optical Care','optical','optical_care_%yyyy-%mm-%dd.txt',null),
(18,4,2,'2011-01-01',null,now(),now(),1,'WPS Health Insurance','WPS','WPS_%yyyy-%mm-%dd.txt',null),
(19,4,2,'2011-01-01',null,now(),now(),1,'Dean Care','dean','dean_care%yyyy-%mm-%dd.txt',null),
(20,5,2,'2011-01-01',null,now(),now(),1,'FSA Express','fsa_express','fsa_express_%yyyy-%mm-%dd.txt',null),
(21,6,2,'2011-01-01',null,now(),now(),1,'Blue Cross Blue Shield Export','bcbs','bcbs%yyyy-%mm-%dd.txt',null),
(22,6,2,'2011-01-01',null,now(),now(),1,'United Health Care','uhc','uhc%yyyy-%mm-%dd.txt',null),
(23,6,2,'2011-01-01',null,now(),now(),1,'Delta Dental','delta','delta_dental_%yyyy-%mm-%dd.txt',null),
(24,7,2,'2011-01-01',null,now(),now(),1,'bSwift Export','bswift','bwsift_%yyyy-%mm-%dd.txt',null),
(25,7,2,'2011-01-01',null,now(),now(),1,'General Ledger Export','General Ledger','%dd_general_ledger%yy-%mm-.txt',null),
(26,8,2,'2011-01-01',null,now(),now(),1,'EyeMed','eyemed','eye_med_%yyyy-%mm-%dd.txt',null),
(27,9,2,'2011-01-01',null,now(),now(),1,'Optical Care','optical','optical_care_%yyyy-%mm-%dd.txt',null),
(28,9,2,'2011-01-01',null,now(),now(),1,'WPS Health Insurance','WPS','WPS_%yyyy-%mm-%dd.txt',null),
(29,10,2,'2011-01-01',null,now(),now(),1,'Dean Care','dean','dean_care%yyyy-%mm-%dd.txt',null),
(30,10,2,'2011-01-01',null,now(),now(),1,'FSA Express','fsa_express','fsa_express_%yyyy-%mm-%dd.txt',null);
/*!40000 ALTER TABLE `export` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `export_schedule` WRITE;
/*!40000 ALTER TABLE `export_schedule` DISABLE KEYS */;
INSERT INTO `export_schedule` VALUES 
(1,1,1,3,'ruby /echoware/code/schedule/master_exporter.rb 1','1 * * * *','cron1','2012-01-01',null,now(),now(),1,'Because'),
(2,1,2,3,'ruby /echoware/code/schedule/master_exporter.rb 2','2 * * * *','cron2','2012-01-01',null,now(),now(),1,'Because'),
(3,1,3,3,'ruby /echoware/code/schedule/master_exporter.rb 3','3 * * * *','cron3','2012-01-01',null,now(),now(),1,'Because'),
(4,1,4,3,'ruby /echoware/code/schedule/master_exporter.rb 4','4 * * * *','cron4','2012-01-01',null,now(),now(),1,'Because'),
(5,1,5,3,'ruby /echoware/code/schedule/master_exporter.rb 5','5 * * * *','cron5','2012-01-01',null,now(),now(),1,'Because'),
(6,1,6,3,'ruby /echoware/code/schedule/master_exporter.rb 6','6 * * * *','cron6','2012-01-01',null,now(),now(),1,'Because'),
(7,1,7,3,'ruby /echoware/code/schedule/master_exporter.rb 7','7 * * * *','cron7','2012-01-01',null,now(),now(),1,'Because'),
(8,1,8,3,'ruby /echoware/code/schedule/master_exporter.rb 8','8 * * * *','cron8','2012-01-01',null,now(),now(),1,'Because'),
(9,1,9,3,'ruby /echoware/code/schedule/master_exporter.rb 9','9 * * * *','cron9','2012-01-01',null,now(),now(),1,'Because'),
(10,1,10,3,'ruby /echoware/code/schedule/master_exporter.rb 10','10 * * * *','cron10','2012-01-01',null,now(),now(),1,'Because'),
(11,1,11,3,'ruby /echoware/code/schedule/master_exporter.rb 11','11 * * * *','cron11','2012-01-01',null,now(),now(),1,'Because'),
(12,1,12,3,'ruby /echoware/code/schedule/master_exporter.rb 12','12 * * * *','cron12','2012-01-01',null,now(),now(),1,'Because'),
(13,1,13,3,'ruby /echoware/code/schedule/master_exporter.rb 13','12 * * * *','cron13','2012-01-01',null,now(),now(),1,'Because'),
(14,1,14,3,'ruby /echoware/code/schedule/master_exporter.rb 14','12 * * * *','cron14','2012-01-01',null,now(),now(),1,'Because'),
(15,1,15,3,'ruby /echoware/code/schedule/master_exporter.rb 15','12 * * * *','cron15','2012-01-01',null,now(),now(),1,'Because'),
(16,1,16,3,'ruby /echoware/code/schedule/master_exporter.rb 16','13 * * * *','cron16','2012-01-01',null,now(),now(),1,'Because'),
(17,1,17,3,'ruby /echoware/code/schedule/master_exporter.rb 17','13 * * * *','cron17','2012-01-01',null,now(),now(),1,'Because'),
(18,1,18,3,'ruby /echoware/code/schedule/master_exporter.rb 18','14 * * * *','cron18','2012-01-01',null,now(),now(),1,'Because'),
(19,1,19,3,'ruby /echoware/code/schedule/master_exporter.rb 19','14 * * * *','cron19','2012-01-01',null,now(),now(),1,'Because'),
(20,1,20,3,'ruby /echoware/code/schedule/master_exporter.rb 20','15 * * * *','cron20','2012-01-01',null,now(),now(),1,'Because'),
(21,1,21,3,'ruby /echoware/code/schedule/master_exporter.rb 21','15 * * * *','cron21','2012-01-01',null,now(),now(),1,'Because'),
(22,1,22,3,'ruby /echoware/code/schedule/master_exporter.rb 22','16 * * * *','cron22','2012-01-01',null,now(),now(),1,'Because'),
(23,1,23,3,'ruby /echoware/code/schedule/master_exporter.rb 23','16 * * * *','cron23','2012-01-01',null,now(),now(),1,'Because'),
(24,1,24,3,'ruby /echoware/code/schedule/master_exporter.rb 24','16 * * * *','cron24','2012-01-01',null,now(),now(),1,'Because'),
(25,1,25,3,'ruby /echoware/code/schedule/master_exporter.rb 25','17 * * * *','cron25','2012-01-01',null,now(),now(),1,'Because'),
(26,1,26,3,'ruby /echoware/code/schedule/master_exporter.rb 26','17 * * * *','cron26','2012-01-01',null,now(),now(),1,'Because'),
(27,1,27,3,'ruby /echoware/code/schedule/master_exporter.rb 27','17 * * * *','cron27','2012-01-01',null,now(),now(),1,'Because'),
(28,1,28,3,'ruby /echoware/code/schedule/master_exporter.rb 28','18 * * * *','cron28','2012-01-01',null,now(),now(),1,'Because'),
(29,1,29,3,'ruby /echoware/code/schedule/master_exporter.rb 29','19 * * * *','cron29','2012-01-01',null,now(),now(),1,'Because'),
(30,1,30,3,'ruby /echoware/code/schedule/master_exporter.rb 30','20 * * * *','cron30','2012-01-01',null,now(),now(),1,'Because');

/*!40000 ALTER TABLE `export_schedule` ENABLE KEYS */;
UNLOCK TABLES;




drop database anyti;
drop database bills;
drop database bobs;
drop database buffa;
drop database carol;
drop database dot;
drop database ihous;
drop database lucky;
drop database targe;
drop database walgr;


create database anyti;
use anyti;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

create database bills;
use bills;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database bobs;
use bobs;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database buffa;
use buffa;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

create database carol;
use carol;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

create database dot;
use dot;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database ihous;
use ihous;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database lucky;
use lucky;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database targe;
use targe;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


create database walgr;
use walgr;

CREATE TABLE `trailer_record` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_bai_id` int(11) DEFAULT NULL,
  `tra_record_type` varchar(45) DEFAULT NULL,
  `tra_count` int(11) DEFAULT NULL,
  `tra_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 ;

CREATE TABLE `employee` (
  `ee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ee_bai_id` int(11) DEFAULT NULL COMMENT 'ID of the batch instance',
  `ee_ees_id` int(11) DEFAULT NULL,
  `ee_source_system_id` int(11) DEFAULT NULL,
  `ee_date_created` datetime DEFAULT NULL,
  `ee_date_modified` datetime DEFAULT NULL,
  `ee_usr_id_modified` int(11) DEFAULT NULL,
  `ee_date_start` datetime DEFAULT NULL COMMENT 'Effective date of this record	',
  `ee_eei_id` int(11) DEFAULT NULL,
  `ee_ssn` varchar(20) DEFAULT NULL,
  `ee_clock` varchar(50) DEFAULT NULL,
  `ee_number` varchar(45) DEFAULT NULL,
  `ee_name_first` varchar(45) DEFAULT NULL,
  `ee_name_last` varchar(45) DEFAULT NULL,
  `ee_name_middle` varchar(45) DEFAULT NULL,
  `ee_name_suffix` varchar(45) DEFAULT NULL,
  `ee_name_prefix` varchar(45) DEFAULT NULL,
  `ee_date_first_hire` datetime DEFAULT NULL,
  `ee_date_last_hire` datetime DEFAULT NULL,
  `ee_date_benefit_calc` datetime DEFAULT NULL,
  `ee_date_terminated` datetime DEFAULT NULL,
  `ee_date_seniority` datetime DEFAULT NULL,
  `ee_term_reason` varchar(50) DEFAULT NULL,
  `ee_employment_category` varchar(50) DEFAULT NULL,
  `ee_full_part_time` varchar(50) DEFAULT NULL,
  `ee_status` varchar(50) DEFAULT NULL,
  `ee_pay_frequency` varchar(50) DEFAULT NULL,
  `ee_pay_type` varchar(50) DEFAULT NULL,
  `ee_hourly_rate` decimal(10,4) DEFAULT NULL,
  `ee_annual_rate` decimal(10,4) DEFAULT NULL,
  `ee_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This field defines, from a company or pay group perspective, how many hours per week this employee gets.',
  `ee_pay_grade` varchar(45) DEFAULT NULL,
  `ee_pay_step` varchar(45) DEFAULT NULL,
  `ee_base_hours_per_week` decimal(3,2) DEFAULT NULL COMMENT 'This defines how many hours per week this employee typically works.  IE part time might work 20 hours per week to calculate pay',
  `ee_date_last_comp_change` datetime DEFAULT NULL,
  `ee_comp_increase_reason` varchar(50) DEFAULT NULL,
  `ee_comp_inrease_amount` decimal(10,4) DEFAULT NULL,
  `ee_company` varchar(50) DEFAULT NULL,
  `ee_division` varchar(50) DEFAULT NULL,
  `ee_region` varchar(50) DEFAULT NULL,
  `ee_location` varchar(50) DEFAULT NULL,
  `ee_department` varchar(50) DEFAULT NULL,
  `ee_pay_group` varchar(50) DEFAULT NULL,
  `ee_title` varchar(125) DEFAULT NULL,
  `ee_phone_work` varchar(45) DEFAULT NULL,
  `ee_date_born` datetime DEFAULT NULL,
  `ee_address_street1` varchar(45) DEFAULT NULL,
  `ee_address_street2` varchar(45) DEFAULT NULL,
  `ee_address_city` varchar(45) DEFAULT NULL,
  `ee_address_state` varchar(45) DEFAULT NULL,
  `ee_address_zip` varchar(45) DEFAULT NULL,
  `ee_country` varchar(45) DEFAULT NULL,
  `ee_phone_home` varchar(45) DEFAULT NULL,
  `ee_phone_cell` varchar(45) DEFAULT NULL,
  `ee_email_home` varchar(45) DEFAULT NULL,
  `ee_ethnic` varchar(50) DEFAULT NULL,
  `ee_sex` varchar(50) DEFAULT NULL,
  `ee_marital` varchar(50) DEFAULT NULL,
  `ee_veteran` int(11) DEFAULT NULL,
  PRIMARY KEY (`ee_id`),
  KEY `ssn` (`ee_ssn`),
  KEY `clock` (`ee_clock`),
  KEY `ee_number` (`ee_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `employee_id` (
  `eei_id` int(11) NOT NULL AUTO_INCREMENT,
  `eei_ssn` varchar(45) DEFAULT NULL,
  `eei_dob` datetime DEFAULT NULL,
  `eei_clock` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eei_id`),
  KEY `ssn` (`eei_ssn`),
  KEY `clock` (`eei_clock`)
) ENGINE=InnoDB AUTO_INCREMENT=1;






