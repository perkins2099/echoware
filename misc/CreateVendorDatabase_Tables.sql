delimiter $$

CREATE DATABASE `cli01` /*!40100 DEFAULT CHARACTER SET latin1 */$$

delimiter $$

use cli01

delimiter $$

CREATE TABLE `imp_vendor1` (
  `imp_id` int(11) NOT NULL AUTO_INCREMENT,
  `imp_tdata` varchar(8000) DEFAULT NULL,
  PRIMARY KEY (`imp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

