CREATE TRIGGER event_time_stamp BEFORE INSERT ON event
FOR EACH ROW
SET NEW.eve_date_created = NOW();
